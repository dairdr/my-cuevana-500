# My cuevana 500

Playing arround with ReactJS

## Project structure

This project contains the next folders (the most important):

- src/
  - _components/_ # Components
  - _containers/_ # Main layout components
  - _networking/_ # HTTP related stuff
  - _store/_ # Redux
  - _styles/_ # Custom styles
  - _utils/_ # As the name suggest
- .gitlab-ci.yml # CI/CD stuff

## System requirements

To complete the setup, please install the [_yarn_](https://yarnpkg.com/en/) package on your machine. otherwise you can use _npm_.

## Install for local development

- Install dependencies

```shell
yarn install
```

- Create a _.env_ file within the root folder and add the environment vars below:

```shell
REACT_APP_API_KEY=0828590896cb6e776c1e401b4182bef1
REACT_APP_BASE_URL=https://api.themoviedb.org/
REACT_APP_IMAES_BASE_URL=https://image.tmdb.org/t/p/w300
```

Note: _I'm giving you the API KEY to save you trouble_

- Run this 💩

```shell
cd <project-root>
yarn start
```
