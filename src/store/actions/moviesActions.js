import Api from '../../networking/api';
import MovieUtils from '../../utils/MovieMapping';

export const fetchPopularMovies = () => dispatch => {
  Api.get('3/movie/popular')
    .then(result => {
      dispatch({
        type: 'FETCH_POPULAR_MOVIES',
        payload: result.data.results
      });
    })
    .catch(error => {
      dispatch({
        type: 'FETCH_POPULAR_MOVIES',
        payload: []
      });
    });
};

export const fetchMovieReviews = id => dispatch => {
  Api.get(`3/movie/${id}/reviews`)
    .then(result => {
      dispatch({
        type: 'FETCH_MOVIE_REVIEWS',
        payload: result.data.results
      });
    })
    .catch(error => {
      dispatch({
        type: 'FETCH_MOVIE_REVIEWS',
        payload: []
      });
    });
};

export const fetchFavorites = () => dispatch => {
  const utils = new MovieUtils();
  const movies = utils.fetchFavorites();
  dispatch({
    type: 'FETCH_FAVORITES',
    payload: movies
  });
};

export const saveFavorite = movie => dispatch => {
  const utils = new MovieUtils();
  let movies = utils.fetchFavorites();
  movies.push(movie);
  localStorage.setItem('favorites', JSON.stringify(movies));

  dispatch({
    type: 'FAVORITE_SAVED',
    payload: movies
  });
};

export const removeFromFavorites = movie => dispatch => {
  const utils = new MovieUtils();
  let movies = utils.fetchFavorites();
  let found = false;
  let index = 0;
  while (!found && index < movies.length) {
    if (movies[index].id === movie.id) {
      found = true;
      break;
    }
    index += 1;
  }

  if (found) {
    movies.splice(index, 1);
    localStorage.setItem('favorites', JSON.stringify(movies));
    dispatch({
      type: 'FAVORITE_DELETED',
      payload: movies
    });
  } else {
    dispatch({
      type: 'FAVORITE_DELETED',
      payload: []
    });
  }
};
