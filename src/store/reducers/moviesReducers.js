import MovieUtils from '../../utils/MovieMapping';

const initialState = {
  popular: [],
  favorites: [],
  reviews: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_POPULAR_MOVIES':
      const popularMovies = action.payload;
      const utils = new MovieUtils();
      const favoritesMovies = utils.fetchFavorites();
      return {
        ...state,
        popular: utils.mapWithFavorite(popularMovies, favoritesMovies)
      };
    case 'FETCH_FAVORITES':
      return {
        ...state,
        favorites: action.payload
      };
    case 'FETCH_MOVIE_REVIEWS':
      return {
        ...state,
        reviews: action.payload
      };
    case 'FAVORITE_SAVED':
    case 'FAVORITE_DELETED':
      return {
        ...state,
        favorites: action.payload
      };
    default:
      return state;
  }
};
