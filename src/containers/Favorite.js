import React from 'react';
import { connect } from 'react-redux';

import { fetchFavorites } from '../store/actions/moviesActions';
import Movie from '../components/Movie';

class Favorite extends React.Component {
  /**
   * Class-based Component lifecycle
   */
  UNSAFE_componentWillMount() {
    this.props.fetchFavorites();
  }

  /**
   * Class-based Component lifecycle to render the component's view
   *
   * @returns {object}
   */
  render() {
    const { favoriteMovies } = this.props;
    const movies = favoriteMovies.map(movie => (
      <Movie key={`${movie.id}`} movie={movie} showFavoriteButton={false} />
    ));

    return (
      <div className="container mt-5">
        {movies.length > 0 ? (
          <div className="row">{movies}</div>
        ) : (
          <div className="alert alert-warning" role="alert">
            Nothing to see, move along, there're not favorites yet
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  favoriteMovies: state.movies.favorites
});

export default connect(
  mapStateToProps,
  { fetchFavorites }
)(Favorite);
