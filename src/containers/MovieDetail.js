import React from 'react';
import { connect } from 'react-redux';

import axios from '../networking/api';
import {
  fetchMovieReviews,
  saveFavorite,
  removeFromFavorites
} from '../store/actions/moviesActions';
import Review from '../components/Review';
import MovieDetailCard from '../components/MovieDetail';
import MovieUtils from '../utils/MovieMapping';

class MovieDetail extends React.Component {
  state = {
    movieDetail: {},
    isFavorite: false
  };

  /**
   * Class-based Component lifecycle
   */
  componentDidMount() {
    this.props.fetchMovieReviews(this.props.match.params.id);
    this.fetchMovieDetail(this.props.match.params.id);
  }

  /**
   * Fetch the movie detail from the REST API
   *
   * @param {array} id The movie's id
   */
  fetchMovieDetail(id) {
    axios
      .get(`3/movie/${id}`)
      .then(response => {
        let movie = response.data;
        const utils = new MovieUtils();
        const favoritesMovies = utils.fetchFavorites();
        movie = utils.mapSingleMovieWithFavorite(movie, favoritesMovies);

        this.setState({
          movieDetail: movie,
          isFavorite: movie.is_favorite
        });
      })
      .catch(error => {});
  }

  /**
   * Add the movie into the Favorite list
   */
  saveFavorite = () => {
    const { saveFavorite } = this.props;
    saveFavorite(this.state.movieDetail);
    this.setState({
      isFavorite: !this.state.isFavorite
    });
  };

  /**
   * Remove the movie from the Favorite list
   */
  deleteFromFavorite = () => {
    const { removeFromFavorites } = this.props;
    removeFromFavorites(this.state.movieDetail);
    this.setState({
      isFavorite: !this.state.isFavorite
    });
  };

  /**
   * Handle the event that trigger add/remove the movie from the Favorite list
   */
  onMark = () => {
    if (this.state.isFavorite) {
      this.deleteFromFavorite();
    } else {
      this.saveFavorite();
    }
  };

  /**
   * Class-based Component lifecycle to render the component's view
   *
   * @returns {object}
   */
  render() {
    const { movieReviews } = this.props;
    const reviews = movieReviews.map(review => (
      <Review key={`${review.id}`} review={review} />
    ));

    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-3">
            {this.state.movieDetail.poster_path && (
              <img
                src={`${process.env.REACT_APP_IMAES_BASE_URL}${
                  this.state.movieDetail.poster_path
                }`}
                alt={'poster'}
                className="pr-2"
              />
            )}
          </div>
          <div className="col">
            <MovieDetailCard
              movieDetail={this.state.movieDetail}
              onMark={this.onMark}
              isFavorite={this.state.isFavorite}
            />
            <h3>Reviews</h3>
            <ul className="list-group">{reviews}</ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  movieReviews: state.movies.reviews
});

export default connect(
  mapStateToProps,
  { fetchMovieReviews, saveFavorite, removeFromFavorites }
)(MovieDetail);
