import React from 'react';
import { connect } from 'react-redux';

import { fetchPopularMovies } from '../store/actions/moviesActions';
import Movie from '../components/Movie';

class Home extends React.Component {
  /**
   * Class-based Component lifecycle
   */
  componentDidMount() {
    this.props.fetchPopularMovies();
  }

  /**
   * Class-based Component lifecycle to render the component's view
   *
   * @returns {object}
   */
  render() {
    const { popularMovies } = this.props;
    const movies = popularMovies.map(movie => (
      <Movie key={`${movie.id}`} movie={movie} showFavoriteButton={true} />
    ));

    return (
      <div className="container mt-5">
        {movies.length > 0 ? (
          <div className="row">{movies}</div>
        ) : (
          <div className="alert alert-warning" role="alert">
            Loading...
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  popularMovies: state.movies.popular
});

export default connect(
  mapStateToProps,
  { fetchPopularMovies }
)(Home);
