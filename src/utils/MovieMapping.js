export default class MovieMapping {
  /**
   * Search for favorites movies among the popular movies
   *
   * @param {array} popularMovies Popular movies
   * @param {array} favoriteMovies Favorites movies
   * @returns {array}
   */
  mapWithFavorite = (popularMovies, favoriteMovies) => {
    return popularMovies.map(movie => {
      return this.mapSingleMovieWithFavorite(movie, favoriteMovies);
    });
  };

  /**
   * Check if a popular movie is in the user's Favorite list
   *
   * @param {object} popularMovie A popular movie to search
   * @param {array} favoriteMovies Favorites movies
   * @returns {object}
   */
  mapSingleMovieWithFavorite = (popularMovie, favoriteMovies) => {
    let found = false;
    let index = 0;
    while (!found && index < favoriteMovies.length) {
      if (favoriteMovies[index].id === popularMovie.id) {
        found = true;
        break;
      }
      index += 1;
    }

    popularMovie.is_favorite = found;
    return popularMovie;
  };

  /**
   * Get the favorites movie from localStorage
   *
   * @returns {array}
   */
  fetchFavorites = () => {
    return JSON.parse(localStorage.getItem('favorites') || '[]');
  };
}
