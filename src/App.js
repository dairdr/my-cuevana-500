import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import Home from './containers/Home';
import Favorite from './containers/Favorite';
import MovieDetail from './containers/MovieDetail';
import NavigationBar from './components/NavigationBar';
import store from './store';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationBar />

        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/favorites" component={Favorite} />
                <Route exact path="/movie/:id" component={MovieDetail} />
              </Switch>
            </div>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;
