import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  saveFavorite,
  removeFromFavorites
} from '../store/actions/moviesActions';

class Movie extends React.Component {
  state = {
    isFavorite: false
  };

  /**
   * Class-based Component lifecycle
   */
  componentDidMount() {
    this.setState({
      isFavorite: this.props.movie.is_favorite
    });
  }

  /**
   * Handle the event that trigger the save movie in the Favorite list
   */
  saveFavorite = () => {
    const { saveFavoriteAction, movie } = this.props;
    saveFavoriteAction(movie);
    this.setState({
      isFavorite: !this.state.isFavorite
    });
  };

  /**
   * Handle the event that trigger the delete movie from the Favorite list
   */
  deleteFromFavorite = () => {
    const { removeFromFavoriteAction, movie } = this.props;
    removeFromFavoriteAction(movie);
    this.setState({
      isFavorite: !this.state.isFavorite
    });
  };

  /**
   * Render the button if needed to mark/unmark the movie as favorite
   *
   * @returns {object}
   */
  renderMarkAsFavoriteButtonIfRequired = () => {
    if (this.props.showFavoriteButton) {
      if (this.state.isFavorite) {
        return (
          <button
            className="btn btn-sm btn-block btn-warning"
            onClick={this.deleteFromFavorite}
          >
            <span className="star">&#9733;</span>&nbsp;Unmark from favorite
          </button>
        );
      } else {
        return (
          <button
            className="btn btn-sm btn-block btn-warning"
            onClick={this.saveFavorite}
          >
            <span className="star">&#9734;</span>&nbsp;Mark as favorite
          </button>
        );
      }
    }
  };

  /**
   * Class-based Component lifecycle to render the component's view
   *
   * @returns {object}
   */
  render() {
    return (
      <div className="col-sm-6 col-md-4 col-lg-3 mt-2">
        <div className="card">
          <img
            src={`${process.env.REACT_APP_IMAES_BASE_URL}${
              this.props.movie.poster_path
            }`}
            className="card-img-top"
            alt={`${this.props.movie.title} poster`}
          />
          <div className="card-body">
            <h5 className="card-title">{this.props.movie.title}</h5>
            <p className="card-text">{`${this.props.movie.overview.substr(
              0,
              50
            )}...`}</p>
            <Link
              to={`/movie/${this.props.movie.id}`}
              className="btn btn-sm btn-block btn-primary"
            >
              Details
            </Link>

            {this.renderMarkAsFavoriteButtonIfRequired()}
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  saveFavoriteAction: movie => dispatch(saveFavorite(movie)),
  removeFromFavoriteAction: movie => dispatch(removeFromFavorites(movie))
});

export default connect(
  null,
  mapDispatchToProps
)(Movie);
