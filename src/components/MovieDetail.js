import React from 'react';

class MovieDetail extends React.Component {
  /**
   * Render the button if needed to mark/unmark the movie as favorite
   *
   * @returns {array}
   */
  renderMarkAsFavoriteButton() {
    if (this.props.isFavorite) {
      return (
        <button className="btn btn-warning btn-lg" onClick={this.props.onMark}>
          <span className="star">&#9733;</span>&nbsp;Unmark from favorite
        </button>
      );
    } else {
      return (
        <button className="btn btn-warning btn-lg" onClick={this.props.onMark}>
          <span className="star">&#9734;</span>&nbsp;Mark as favorite
        </button>
      );
    }
  }

  /**
   * Class-based Component lifecycle to render the component's view
   *
   * @returns {object}
   */
  render() {
    return (
      <div className="jumbotron">
        <button type="button" className="btn btn-primary btn-sm">
          Popularity&nbsp;
          <span className="badge badge-light">
            {this.props.movieDetail.popularity}
          </span>
        </button>
        <h1 className="display-4">{this.props.movieDetail.title}</h1>
        <i>{this.props.movieDetail.tagline}</i>
        <p className="lead">{this.props.movieDetail.overview}</p>
        <hr className="my-4" />
        {this.renderMarkAsFavoriteButton()}
      </div>
    );
  }
}

export default MovieDetail;
