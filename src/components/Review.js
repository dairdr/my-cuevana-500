import React from 'react';

export default props => {
  return (
    <li className="list-group-item">
      <div>
        <span className="badge badge-success">{props.review.author}</span>
      </div>
      <div>{props.review.content}</div>
    </li>
  );
};
